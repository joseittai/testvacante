<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Asignacion extends CI_Controller
{
    public function listaAsignaciones()
    {
        $this->load->model('AsignacionModel');
        $asignaciones = $this->AsignacionModel->getAsignaciones();
        return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($asignaciones));

    }

    public function saveAsignacion(){
        $asignacion = json_decode(file_get_contents('php://input'));
        $this->load->model('AsignacionModel');
        $this->AsignacionModel->saveAsignacion($asignacion);
    }

    public function deleteAsignacion(){
        $asignacion = json_decode(file_get_contents('php://input'));
        $this->load->model('AsignacionModel');
        $this->AsignacionModel->deleteAsignacion($asignacion);
    }


}