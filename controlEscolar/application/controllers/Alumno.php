<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Alumno extends CI_Controller
{
    public function listaAlumnos()
    {
        $this->load->model('AlumnoModel');
        $alumnos = $this->AlumnoModel->getAlumnos();
        return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($alumnos));

    }
    public function saveAlumno(){
        $alumno = json_decode(file_get_contents('php://input'));
        $this->load->model('AlumnoModel');
        $this->AlumnoModel->saveAlumno($alumno);
    }

    public function updateAlumno(){
        $alumno = json_decode(file_get_contents('php://input'));
        $this->load->model('AlumnoModel');
        $this->AlumnoModel->updateAlumno($alumno);
    }

    public function deleteAlumno(){
        $alumno = json_decode(file_get_contents('php://input'));
        $this->load->model('AlumnoModel');
        $valida = $this->AlumnoModel->validaAlumno($alumno);
        if($valida[0]->no_regs > 0){
            return $this->output
            ->set_status_header(409)
            ->set_content_type('application/json')
            ->set_output(json_encode(array('msj'=> 'No se puede eliminar el alumno, ya tiene materias asignadas')));
        }
        $this->AlumnoModel->deleteAlumno($alumno);
    }
    
}