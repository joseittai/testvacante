<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Materia extends CI_Controller
{
    public function listMaterias()
    {
        $this->load->model('MateriaModel');
        $materias = $this->MateriaModel->getMaterias();
        return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($materias));

    }

    public function saveMateria(){
        $materia = json_decode(file_get_contents('php://input'));
        $this->load->model('MateriaModel');
        $this->MateriaModel->saveMateria($materia);
    }

    public function updateMateria(){
        $materia = json_decode(file_get_contents('php://input'));
        $this->load->model('MateriaModel');
        $this->MateriaModel->updateMateria($materia);
    }

    public function deleteMateria(){
        $materia = json_decode(file_get_contents('php://input'));
        $this->load->model('MateriaModel');
        $valida = $this->MateriaModel->validaMateria($materia);
        if($valida[0]->no_regs > 0){
            return $this->output
            ->set_status_header(409)
            ->set_content_type('application/json')
            ->set_output(json_encode(array('msj'=> 'No se puede eliminar la materia, ya se encuentra asignada al menos a un alumno')));
        }

        $this->MateriaModel->deleteMateria($materia);
    }

}