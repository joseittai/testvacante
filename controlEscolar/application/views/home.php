<!DOCTYPE html>
<html>
<head>
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/@mdi/font@6.x/css/materialdesignicons.min.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
  <title>Control Escolar</title>
</head>
<body>
  <div id="app">
    <template>
      <v-app id="inspire">
        <v-navigation-drawer
          v-model="drawer"
          app
          absolute
          temporary
        >
        <v-list>
        <v-list-item>
          <v-list-item-content>
            <v-list-item-title class="font-weight-bold">Menú</v-list-item-title>
          </v-list-item-content>
        </v-list-item>
      <v-divider></v-divider>
          <v-list-item
            v-for="item in items"
            :key="item.title"
            link
          >
            <v-list-item-icon>
              <v-icon color="#9d7a13">{{ item.icon }}</v-icon>
            </v-list-item-icon>

            <v-list-item-content @click="seteaMenu(item.opt)">
              <v-list-item-title class="font-weight-medium">{{ item.title }}</v-list-item-title>
            </v-list-item-content>
          </v-list-item>
        </v-list>
        </v-navigation-drawer>

        <v-app-bar app color="#1b396a" dark>
          <v-app-bar-nav-icon @click="drawer = !drawer"></v-app-bar-nav-icon>

          <v-toolbar-title>Control Escolar</v-toolbar-title>
        </v-app-bar>

        <v-main>
          <v-container>
          <!--Panel Inicial-->
          <v-row align="start" fluid v-show="viewerPanel">
            <v-container fluid>
              <v-row dense>
                <v-row
                  align="center"
                  justify="center">
                  <img src="https://preparatoriaoficial267reginaavalos.com.mx/uploads/s/s/f/a/sfadlysvmdpt/img/autocrop/8c1a8da918112c3b2ff8ec0dd7927375.png" width="80%" height="80%">
                </v-row>
              </v-row>
            </v-container>
          </v-row>
          <!--Fin Panel Inicial -->
          <!--Panel Alumnos-->
          <v-row align="start" v-show="viewerAlumnos">
            <v-flex md12>
                <v-card>
                  <v-card-title>
                    <v-icon color="#9d7a13" size="25">mdi-account</v-icon> 
                    Catálogo de Alumnos
                    <v-divider
                      class="mx-4"
                      inset
                      vertical
                    ></v-divider>
                    <v-spacer></v-spacer>
                    <v-text-field
                      v-model="searchAlumno"
                      append-icon="mdi-magnify"
                      label="Buscador"
                      hint="Puedes buscar por cualquier dato del alumno"
                    ></v-text-field>
                  </v-card-title>
                  <v-col cols="12">
                  <v-tooltip top>
                      <template v-slot:activator="{ on, attrs }">
                        <v-btn small
                          color="success"
                          class="white--text"
                          v-on="on"
                          @click="dialogAddAlumno = true"
                        >
                        <v-icon left dark>mdi-plus-circle</v-icon>
                          Agregar
                        </v-btn>
                      </template>
                      <span>Agregar Alumno</span>
                    </v-tooltip>
                    &nbsp;
                    <v-tooltip top>
                      <template v-slot:activator="{ on, attrs }">
                        <v-btn small
                          color="#607D8B"
                          class="white--text"
                          v-on="on"
                          @click="getListAlumnos"
                        >
                        <v-icon left dark>mdi-refresh</v-icon>
                          Actualizar
                        </v-btn>
                      </template>
                      <span>Actualizar</span>
                    </v-tooltip>
                  </v-col>
                  <v-data-table
                    :headers="headerAlumnos"
                    :items="rowDataAlumnos"
                    :search="searchAlumno"
                  >
                    <template v-slot:item.actions="{ item }">
                      <v-tooltip right>
                        <template v-slot:activator="{ on, attrs }">
                          <v-icon
                            class="mr-2"
                            color="#1b396a"
                            v-on="on"
                            @click="openModalUpdateAlumno(item)"
                          >
                            mdi-pencil
                          </v-icon>
                        </template>
                        <span>Editar</span>
                      </v-tooltip>
                      <v-tooltip right>
                        <template v-slot:activator="{ on, attrs }">
                          <v-icon
                          class="mr-2"
                          color="red"
                          v-on="on"
                          @click="openDeleteAlumno(item)"
                          >
                            mdi-delete
                          </v-icon>
                        </template>
                        <span>Eliminar Alumno</span>
                      </v-tooltip>
                    </template>
                  </v-data-table>
                </v-card>
            </v-flex>
          </v-row>

          <v-dialog v-model="dialogAddAlumno" persistent max-width="40%">
            <v-card>
              <v-card-title class="blue-grey darken-1 py-4 white--text">
                <span style="font-size: 1em" v-show="idAlumno == null">
                  <v-icon left dark>mdi-plus-circle</v-icon> Agregar Alumno
                </span>
                <span style="font-size: 1em" v-show="idAlumno != null">
                <v-icon left dark>mdi-pencil</v-icon> Editar Alumno
                </span>
              </v-card-title>
              <v-card-text>
                <v-container>
                  <v-row>
                    <v-col cols="12" sm="12" md="12">
                      <v-text-field label="Matricula" prepend-icon="mdi-card-bulleted" v-model="matricula" counter="4" maxlength="4"></v-text-field>
                    </v-col>
                    <v-col cols="12" sm="12" md="12">
                      <v-text-field label="Nombre" prepend-icon="mdi-account-circle" v-model="nombreAlumno" counter="250" maxlength="25"></v-text-field>
                    </v-col>
                    <v-col cols="12" sm="12" md="12">
                      <v-menu
                        v-model="menu2"
                        :close-on-content-click="false"
                        :nudge-right="40"
                        transition="scale-transition"
                        offset-y
                        min-width="auto"
                      >
                        <template v-slot:activator="{ on, attrs }">
                          <v-text-field
                            v-model="date"
                            label="Fecha de Registro"
                            prepend-icon="mdi-calendar"
                            readonly
                            v-bind="attrs"
                            v-on="on"
                          ></v-text-field>
                        </template>
                        <v-date-picker
                          v-model="date"
                          @input="menu2 = false"
                          locale="es"
                        ></v-date-picker>
                     </v-menu>
                    </v-col>
                                        
                  </v-row>
                </v-container>
              </v-card-text>
              <v-card-actions>
                <v-spacer></v-spacer>
                <v-btn color="red" text @click="closeModalAddAlumno">Cerrar</v-btn>
                  <v-btn color="success" text @click="saveAlumno" v-show="idAlumno == null">
                    <span>Guardar</span>
                  </v-btn>
                  <v-btn color="success" text @click="updateAlumno" v-show="idAlumno != null">
                    <span>Guardar Cambios</span>
                  </v-btn>
              </v-card-actions>
              </v-card>
          </v-dialog>

          <!-- Fin Panel Alumnos-->

          <!--Panel Materias-->
          <v-row align="start" v-show="viewerMaterias">
            <v-flex md12>
                <v-card class="mt-3">
                  <v-card-title>
                  <v-icon color="#9d7a13" size="25">mdi-bookshelf</v-icon>
                    Catálogo de Materias 
                    <v-divider
                      class="mx-4"
                      inset
                      vertical
                    ></v-divider>
                    <v-spacer></v-spacer>
                    <v-text-field
                      v-model="searchMaterias"
                      append-icon="mdi-magnify"
                      label="Buscador"
                      hint="Puedes buscar por cualquier dato de la materia"
                    ></v-text-field>
                  </v-card-title>
                  <v-col cols="12">
                    <v-tooltip top>
                      <template v-slot:activator="{ on, attrs }">
                        <v-btn small
                          color="success"
                          class="white--text"
                          v-on="on"
                          @click="dialogAddMateria = true"
                        >
                        <v-icon left dark>mdi-plus-circle</v-icon>
                          Agregar
                        </v-btn>
                      </template>
                      <span>Agregar Materias</span>
                    </v-tooltip>
                    &nbsp;
                    <v-tooltip top>
                      <template v-slot:activator="{ on, attrs }">
                        <v-btn small
                          color="#607D8B"
                          class="white--text"
                          v-on="on"
                          @click="getListMaterias"
                        >
                        <v-icon left dark>mdi-refresh</v-icon>
                          Actualizar
                        </v-btn>
                      </template>
                      <span>Actualizar</span>
                    </v-tooltip>
                    </v-col>
                  <v-data-table
                    :headers="headerMaterias"
                    :items="rowDataMaterias"
                    :search="searchMaterias"
                  >
                    <template v-slot:item.actions="{ item }">
                      <v-tooltip right>
                        <template v-slot:activator="{ on, attrs }">
                          <v-icon
                            class="mr-2"
                            color="#1b396a"
                            v-on="on"
                            @click="openModalUpdateMateria(item)"
                          >
                            mdi-pencil
                          </v-icon>
                        </template>
                        <span>Editar</span>
                      </v-tooltip>
                      <v-tooltip right>
                        <template v-slot:activator="{ on, attrs }">
                          <v-icon
                          class="mr-2"
                          color="red"
                          v-on="on"
                          @click="openDeleteMateria(item)"
                          >
                            mdi-delete
                          </v-icon>
                        </template>
                        <span>Eliminar Materia</span>
                      </v-tooltip>
                    </template>
                  </v-data-table>
                </v-card>
            </v-flex>
          </v-row>

          <v-dialog v-model="dialogAddMateria" persistent max-width="40%">
            <v-card>
              <v-card-title class="blue-grey darken-1 py-4 white--text">
                <span style="font-size: 1em" v-show="idMateria == null">
                  <v-icon left dark>mdi-plus-circle</v-icon> Agregar Materia
                </span>
                <span style="font-size: 1em" v-show="idMateria != null">
                <v-icon left dark>mdi-pencil</v-icon> Editar Materia
                </span>
              </v-card-title>
              <v-card-text>
                <v-container>
                  <v-row>
                    <v-col cols="12" sm="12" md="12">
                      <v-text-field label="Clave Materia" prepend-icon="mdi-card-bulleted" v-model="claveMateria" counter="4" maxlength="4"></v-text-field>
                    </v-col>
                    <v-col cols="12" sm="12" md="12">
                      <v-text-field label="Nombre Materia" prepend-icon="mdi-bookshelf" v-model="nombreMateria" counter="100" maxlength="100"></v-text-field>
                    </v-col>            
                  </v-row>
                </v-container>
              </v-card-text>
              <v-card-actions>
                <v-spacer></v-spacer>
                <v-btn color="red" text @click="closeModalAddMateria">Cerrar</v-btn>
                  <v-btn color="success" text @click="saveMateria" v-show="idMateria == null">
                    <span>Guardar</span>
                  </v-btn> 
                  <v-btn color="success" text @click="updateMateria" v-show="idMateria != null">
                    <span>Guardar Cambios</span>
                  </v-btn>
              </v-card-actions>
              </v-card>
          </v-dialog>
          <!-- Fin Panel Materias-->

          <!--Panel Asignacion-->
          <v-row align="start" v-show="viewerAsignacion">
            <v-flex md12>
                <v-card class="mt-3">
                  <v-card-title>
                  <v-icon color="#9d7a13" size="25">mdi-clipboard-account</v-icon> Asignación Materia - Alumno
                  <v-divider
                      class="mx-4"
                      inset
                      vertical
                    ></v-divider>
                    <v-spacer></v-spacer>
                    <v-text-field
                      v-model="searchAsignacion"
                      append-icon="mdi-magnify"
                      label="Buscador"
                      hint="Puedes buscar por cualquier dato de la asignación"
                    ></v-text-field>
                  </v-card-title>
                  <v-col cols="12">
                  <v-tooltip top>
                      <template v-slot:activator="{ on, attrs }">
                        <v-btn small
                          color="success"
                          class="white--text"
                          v-on="on"
                          @click="dialogAddAsignacion = true"
                        >
                        <v-icon left dark>mdi-plus-circle</v-icon>
                          Agregar
                        </v-btn>
                      </template>
                      <span>Agregar Asignación</span>
                    </v-tooltip>
                    &nbsp;
                    <v-tooltip top>
                      <template v-slot:activator="{ on, attrs }">
                        <v-btn small
                          color="#607D8B"
                          class="white--text"
                          v-on="on"
                          @click="getListAsignaciones"
                        >
                        <v-icon left dark>mdi-refresh</v-icon>
                          Actualizar
                        </v-btn>
                      </template>
                      <span>Actualizar</span>
                    </v-tooltip>
                  </v-col>
                  <v-data-table
                    :headers="headerAsignacion"
                    :items="rowDataAsigacion"
                    :search="searchAsignacion"
                  >
                    <template v-slot:item.actions="{ item }">
                      <v-tooltip right>
                        <template v-slot:activator="{ on, attrs }">
                          <v-icon
                          class="mr-2"
                          color="red"
                          v-on="on"
                          @click="openDeleteAsignacion(item)"
                          >
                            mdi-delete
                          </v-icon>
                        </template>
                        <span>Eliminar Asignación</span>
                      </v-tooltip>
                    </template>
                  </v-data-table>
                </v-card>
            </v-flex>
          </v-row>
          <!-- Fin Panel Asignacion-->

          <v-dialog v-model="dialogAddAsignacion" persistent max-width="40%">
            <v-card>
              <v-card-title class="blue-grey darken-1 py-4 white--text">
                <span style="font-size: 1em" >
                  <v-icon left dark>mdi-plus-circle</v-icon> Agregar Asignación
                </span>
              </v-card-title>
              <v-card-text>
                <v-container>
                  <v-row>
                    <v-col cols="12" sm="12" md="12">
                      <v-autocomplete placeholder="Alumno" :items="rowDataAlumnos"  item-text="nombre" item-value="id" prepend-icon="mdi-account" v-model="idAlumno"></v-autocomplete>
                    </v-col>
                    <v-col cols="12" sm="12" md="12">
                      <v-autocomplete placeholder="Materia" :items="rowDataMaterias"  item-text="nombre" item-value="id" prepend-icon="mdi-bookshelf" v-model="idMateria"></v-autocomplete>
                    </v-col>            
                  </v-row>
                </v-container>
              </v-card-text>
              <v-card-actions>
                <v-spacer></v-spacer>
                <v-btn color="red" text @click="closeModalAddAsignacion">Cerrar</v-btn>
                  <v-btn color="success" text @click="saveAsignacion">
                    <span>Guardar</span>
                  </v-btn>     
              </v-card-actions>
              </v-card>
          </v-dialog>
          
          </v-container>
          <v-snackbar
            v-model="snackbar"
            top
            :color="colorSnack"
            :timeout="timeout"
          >
            {{ textSnack }}

            <template v-slot:action="{ attrs }">
              <v-btn
                color="re"
                text
                v-bind="attrs"
                @click="snackbar = false"
              >
                <v-icon>mdi-close</v-icon>
              </v-btn>
            </template>
          </v-snackbar>
          <v-dialog v-model="dialogMensajeAlumno" persistent max-width="290">
            <v-card>
              <v-card-title class="headline">Eliminar Alumno</v-card-title>
                <v-card-text>El Alumno # {{idAlumno}} será eliminado ¿Desea Continuar?</v-card-text>
                  <v-card-actions>
                    <v-spacer></v-spacer>
                    <v-btn color="red" text @click="closeModalEstatusAlumno">Cancelar</v-btn>
                    <v-btn color="green" text @click="deleteAlumno">Aceptar</v-btn>
                  </v-card-actions>
            </v-card>
          </v-dialog>
          <v-dialog v-model="dialogMensajeMateria" persistent max-width="290">
            <v-card>
              <v-card-title class="headline">Eliminar Materia</v-card-title>
                <v-card-text>La materia # {{idMateria}} será eliminada ¿Desea Continuar?</v-card-text>
                  <v-card-actions>
                    <v-spacer></v-spacer>
                    <v-btn color="red" text @click="closeModalEstatusMateria">Cancelar</v-btn>
                    <v-btn color="green" text @click="deleteMateria">Aceptar</v-btn>
                  </v-card-actions>
            </v-card>
          </v-dialog>
          <v-dialog v-model="dialogMensajeAsignacion" persistent max-width="290">
            <v-card>
              <v-card-title class="headline">Eliminar Asignación</v-card-title>
                <v-card-text>La Asignación # {{idAsignacion}} será eliminada ¿Desea Continuar?</v-card-text>
                  <v-card-actions>
                    <v-spacer></v-spacer>
                    <v-btn color="red" text @click="closeModalEstatusAsignacion">Cancelar</v-btn>
                    <v-btn color="green" text @click="deleteAsignacion">Aceptar</v-btn>
                  </v-card-actions>
            </v-card>
          </v-dialog>
        </v-main>
      </v-app>
    </template>
  </div>

 
  <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>

  <script>
    new Vue({
      el: '#app',
      vuetify: new Vuetify(),
      data: () => ({
        drawer: false,
        items: [
          { title: 'Alumnos', icon: 'mdi-account', opt: 1 },
          { title: 'Materias', icon: 'mdi-bookshelf', opt: 2  },
          { title: 'Asignaciones', icon: 'mdi-clipboard-account', opt: 3  },
        ],
        viewerPanel: true,
        viewerAlumnos: false,
        viewerMaterias: false,
        viewerAsignacion: false,
        urlRoot: 'http://localhost/controlEscolar/index.php/',
        dialogAddAlumno: false,
        date: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0, 10),
        menu2: false,
        matricula: null,
        nombreAlumno: null,
        dialogAddMateria: false,
        claveMateria: null,
        nombreMateria: null,
        headerAlumnos: [
          {text: 'ID',align: 'start', value: 'id', class: ['blue-grey darken-1', 'white--text']},
          { text: 'MATRICULA', value: 'matricula', class: ['blue-grey darken-1', 'white--text'] },
          { text: 'NOMBRE', value: 'nombre', class: ['blue-grey darken-1', 'white--text'] },
          { text: 'FECHA DE REGISTRO', value: 'fecha_registro', class: ['blue-grey darken-1', 'white--text'] },
          { text: 'OPCIONES', value: 'actions', sortable: false, class: ['blue-grey darken-1', 'white--text'] }
        ],
        rowDataAlumnos: [],
        searchAlumno: '',
        idAlumno: null,
        dialogMensajeAlumno: false,
        headerMaterias: [
          {text: 'ID',align: 'start', value: 'id', class: ['blue-grey darken-1', 'white--text']},
          { text: 'CLAVE MATERIA', value: 'clave', class: ['blue-grey darken-1', 'white--text'] },
          { text: 'NOMBRE MATERIA', value: 'nombre', class: ['blue-grey darken-1', 'white--text'] },
          { text: 'OPCIONES', value: 'actions', sortable: false, class: ['blue-grey darken-1', 'white--text'] }
        ],
        rowDataMaterias: [],
        searchMaterias: '',
        idMateria: null,
        dialogMensajeMateria: false,
        headerAsignacion: [
          {text: 'ID',align: 'start', value: 'id', class: ['blue-grey darken-1', 'white--text']},
          { text: 'MATRICULA', value: 'matricula', class: ['blue-grey darken-1', 'white--text'] },
          { text: 'NOMBRE ALUMNO', value: 'nombre', class: ['blue-grey darken-1', 'white--text'] },
          { text: 'NOMBRE MATERIA', value: 'nombre_materia', class: ['blue-grey darken-1', 'white--text'] },
          { text: 'CLAVE MATERIA', value: 'clave', class: ['blue-grey darken-1', 'white--text'] },
          { text: 'OPCIONES', value: 'actions', sortable: false, class: ['blue-grey darken-1', 'white--text'] }
        ],
        rowDataAsigacion: [],
        searchAsignacion: '',
        dialogAddAsignacion: false,
        dialogMensajeAsignacion: false,
        idAsignacion: null,
        /////////////////////////
        snackbar: false,
        colorSnack: '',
        textSnack: '',
        timeout: 3000
      }),
      methods: {
        seteaMenu(opt) {
          if(opt == 1){
            vm.viewerPanel = false
            vm.viewerAlumnos = true
            vm.viewerMaterias = false
            vm.viewerAsignacion = false
            vm.drawer = false
          }
          if(opt == 2){
            vm.viewerPanel = false
            vm.viewerAlumnos = false
            vm.viewerMaterias = true
            vm.viewerAsignacion = false
            vm.drawer = false
          }
          if(opt == 3){
            vm.viewerPanel = false
            vm.viewerAlumnos = false
            vm.viewerMaterias = false
            vm.viewerAsignacion = true
            vm.drawer = false
          }
        },
        closeModalAddAlumno(){
          vm.dialogAddAlumno = false
          vm.matricula = null
          vm.nombreAlumno =  null
          vm.idAlumno = null
        },
        closeModalAddMateria(){
          vm.dialogAddMateria = false
          vm.claveMateria = null,
          vm.nombreMateria = null
        },
       
        saveAlumno(){
          if(vm.matricula == null || vm.matricula == '' || vm.matricula.length < 4){
            vm.colorSnack = 'red'
            vm.textSnack = 'Ingrese una Matricula válida'
            vm.snackbar = true
            return false
          }
          if(vm.nombreAlumno == null || vm.nombreAlumno == '' || vm.nombreAlumno.length > 250){
            vm.colorSnack = 'red'
            vm.textSnack = 'Ingrese un nombre válido'
            vm.snackbar = true
            return false
          }
          axios.post(vm.urlRoot+'save/alumnos', {
            matricula: vm.matricula,
            nombre: vm.nombreAlumno,
            fechaIngreso: vm.date
          }).then(response => {
            vm.openSnackbar('green','Alumno Guardado con éxito')
            vm.closeModalAddAlumno()
            vm.getListAlumnos()
          }).catch(e => {
            console.log(e);
          });
        },
        updateAlumno(){
          if(vm.matricula == null || vm.matricula == '' || vm.matricula.length < 4){
            vm.colorSnack = 'red'
            vm.textSnack = 'Ingrese una Matricula válida'
            vm.snackbar = true
            return false
          }
          if(vm.nombreAlumno == null || vm.nombreAlumno == '' || vm.nombreAlumno.length > 250){
            vm.colorSnack = 'red'
            vm.textSnack = 'Ingrese un nombre válido'
            vm.snackbar = true
            return false
          }
          axios.post(vm.urlRoot+'update/alumnos', {
            matricula: vm.matricula,
            nombre: vm.nombreAlumno,
            fechaIngreso: vm.date,
            id: vm.idAlumno
          }).then(response => {
            vm.openSnackbar('green','Alumno Actualizado con éxito')
            vm.closeModalAddAlumno()
            vm.getListAlumnos()
          }).catch(e => {
            console.log(e);
          });
        },
        getListAlumnos(){
          axios.get(vm.urlRoot+'get/alumnos', {}).then(function (res) {
              vm.overlay = false
          if (res.status == 200) {
            vm.rowDataAlumnos = res.data
          }
          }).catch(function (err) {
                console.log(err)
          })
        },
        openModalUpdateAlumno(item){
          vm.idAlumno = item.id
          vm.matricula = item.matricula
          vm.nombreAlumno = item.nombre
          vm.date = item.fecha_registro
          vm.dialogAddAlumno = true
        },
        openDeleteAlumno(item){
          vm.idAlumno = item.id
          vm.dialogMensajeAlumno = true
        },
        closeModalEstatusAlumno(){
          vm.idAlumno = null
          vm.dialogMensajeAlumno = false
        },
        deleteAlumno(){
          axios.post(vm.urlRoot+'delete/alumnos', {
            id: vm.idAlumno
          }).then(response => {
            vm.openSnackbar('green','Alumno eliminado con éxito')
            vm.closeModalEstatusAlumno()
            vm.getListAlumnos()
          }).catch(e => {
            vm.closeModalEstatusAlumno()
            vm.openSnackbar('red',e.response.data.msj)
          });
        },
        getListMaterias(){
          axios.get(vm.urlRoot+'get/materias', {}).then(function (res) {
              vm.overlay = false
          if (res.status == 200) {
            vm.rowDataMaterias = res.data
          }
          }).catch(function (err) {
                console.log(err)
          })
        },
        saveMateria(){
          if(vm.claveMateria == null || vm.claveMateria == '' || vm.claveMateria < 4){
            vm.colorSnack = 'red'
            vm.textSnack = 'Ingrese una Clave de Materia válida'
            vm.snackbar = true
            return false
          }
          if(vm.nombreMateria == null || vm.nombreMateria == '' || vm.nombreMateria.length > 250){
            vm.colorSnack = 'red'
            vm.textSnack = 'Ingrese un nombre válido'
            vm.snackbar = true
            return false
          }
          axios.post(vm.urlRoot+'save/materias', {
            clave: vm.claveMateria,
            nombre: vm.nombreMateria
          }).then(response => {
            vm.openSnackbar('green','Materia Generada con éxito')
            vm.closeModalAddMateria()
            vm.getListMaterias()
          }).catch(e => {
            console.log(e);
          });
        },
        openModalUpdateMateria(item){
          vm.idMateria = item.id
          vm.claveMateria = item.clave
          vm.nombreMateria = item.nombre
          vm.dialogAddMateria = true
        },
        updateMateria(){
          if(vm.claveMateria == null || vm.claveMateria == '' || vm.claveMateria < 4){
            vm.colorSnack = 'red'
            vm.textSnack = 'Ingrese una Clave de Materia válida'
            vm.snackbar = true
            return false
          }
          if(vm.nombreMateria == null || vm.nombreMateria == '' || vm.nombreMateria.length > 250){
            vm.colorSnack = 'red'
            vm.textSnack = 'Ingrese un nombre válido'
            vm.snackbar = true
            return false
          }
          axios.post(vm.urlRoot+'update/materias', {
            clave: vm.claveMateria,
            nombre: vm.nombreMateria,
            id: vm.idMateria
          }).then(response => {
            vm.openSnackbar('green','Materia Actualizada con éxito')
            vm.closeModalAddMateria()
            vm.getListMaterias()
          }).catch(e => {
            console.log(e);
          });
        },
        openDeleteMateria(item){
          vm.idMateria = item.id
          vm.dialogMensajeMateria = true
        },
        closeModalEstatusMateria(){
          vm.idMateria = null
          vm.dialogMensajeMateria = false
        },
        openSnackbar(color,texto){
          vm.snackbar = true
          vm.colorSnack = color
          vm.textSnack = texto
        },
        deleteMateria(){
          axios.post(vm.urlRoot+'delete/materias', {
            id: vm.idMateria
          }).then(response => {
            vm.openSnackbar('green','Materia eliminada con éxito')
            vm.closeModalEstatusMateria()
            vm.getListMaterias()
          }).catch(e => {
            vm.closeModalEstatusMateria()
            vm.openSnackbar('red',e.response.data.msj)
          });
        },
        closeModalAddAsignacion(){
          vm.dialogAddAsignacion = false
          vm.idAlumno = null,
          vm.idMateria = null
        },
        saveAsignacion(){
          if(vm.idAlumno == null || vm.idAlumno == ''){
            vm.colorSnack = 'red'
            vm.textSnack = 'Seleccione un alumno'
            vm.snackbar = true
            return false
          }
          if(vm.idMateria == null || vm.idMateria == ''){
            vm.colorSnack = 'red'
            vm.textSnack = 'Seleccione una materia'
            vm.snackbar = true
            return false
          }
          axios.post(vm.urlRoot+'save/asignaciones', {
            idAlumno: vm.idAlumno,
            idMateria: vm.idMateria
          }).then(response => {
            vm.openSnackbar('green','Asignación Generada con éxito')
            vm.closeModalAddAsignacion()
            vm.getListAsignaciones()
          }).catch(e => {
            console.log(e);
          });
        },
        getListAsignaciones(){
          axios.get(vm.urlRoot+'get/asignaciones', {}).then(function (res) {
              vm.overlay = false
          if (res.status == 200) {
            vm.rowDataAsigacion = res.data
          }
          }).catch(function (err) {
                console.log(err)
          })
        },
        openDeleteAsignacion(item){
          vm.idAsignacion = item.id
          vm.dialogMensajeAsignacion = true
        },
        closeModalEstatusAsignacion(){
          vm.idAsignacion = null
          vm.dialogMensajeAsignacion = false
        },
        deleteAsignacion(){
          axios.post(vm.urlRoot+'delete/asignacion', {
            id: vm.idAsignacion
          }).then(response => {
            vm.openSnackbar('green','Asignación eliminada con éxito')
            vm.closeModalEstatusAsignacion()
            vm.getListAsignaciones()
          }).catch(e => {
            console.log(e);
          });
        },

      },
      mounted() {
        window.vm = this
        vm.getListAlumnos()
        vm.getListMaterias()
        vm.getListAsignaciones()
      }
    })
  </script>
</body>
</html>