<?php
class AsignacionModel extends CI_Model
{
    public $id_alumno;
    public $id_materia;
    public function getAsignaciones()
    {
        $this->load->database();
        $query = $this->db->query('
        SELECT
            asignaciones.id,
            alumnos.matricula,
            alumnos.nombre,
            materias.nombre as nombre_materia,
            materias.clave
        FROM asignaciones
        inner join alumnos on alumnos.id = asignaciones.id_alumno
        inner join materias on materias.id = asignaciones.id_materia
        ORDER BY alumnos.fecha_registro DESC');
        return $query->result();
    }

    public function saveAsignacion($asignacion)
    {
        $this->load->database();
        $this->id_alumno = $asignacion->idAlumno;
        $this->id_materia = $asignacion->idMateria;
        $this->db->insert('asignaciones', $this);
    }

    public function deleteAsignacion($asignacion)
    {
        $id = $asignacion->id;
        $this->load->database();
        $this->db->where('id', $id);
        $this->db->delete('asignaciones');

    }
}