<?php 
class MateriaModel extends CI_Model
{
    public $clave;
    public $nombre;
    public function getMaterias()
    {
        $this->load->database();
        $query = $this->db->get('materias');
        return $query->result();
    }
    
    public function saveMateria($materia)
    {
        $this->load->database();
        $this->clave = $materia->clave;
        $this->nombre = $materia->nombre;
        $this->db->insert('materias', $this);

    }

    public function updateMateria($materia)
    {
        $this->load->database();
        $this->clave = $materia->clave;
        $this->nombre = $materia->nombre;
        $id = $materia->id;
        $this->db->update('materias', $this, array('id' => $id));

    }

    public function deleteMateria($materia)
    {
        $id = $materia->id;
        $this->load->database();
        $this->db->where('id', $id);
        $this->db->delete('materias');

    }

    public function validaMateria($materia)
    {
        $this->load->database();
        $id = $materia->id;
        $query = $this->db->query('SELECT COUNT(id) as no_regs FROM asignaciones where asignaciones.id_materia = '.$id);
        return $query->result();
    }
}