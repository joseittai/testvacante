<?php 
class AlumnoModel extends CI_Model
{
    public $matricula;
    public $nombre;
    public $fecha_registro;
    public function getAlumnos()
    {
        $this->load->database();
        $query = $this->db->get('alumnos');
        return $query->result();
    }

    public function saveAlumno($alumno)
    {
        $this->load->database();
        $this->matricula = $alumno->matricula;
        $this->nombre = $alumno->nombre;
        $this->fecha_registro = $alumno->fechaIngreso;
        $this->db->insert('alumnos', $this);

    }

    public function updateAlumno($alumno)
    {
        $this->load->database();
        $this->matricula = $alumno->matricula;
        $this->nombre = $alumno->nombre;
        $this->fecha_registro = $alumno->fechaIngreso;
        $id = $alumno->id;
        $this->db->update('alumnos', $this, array('id' => $id));

    }

    public function deleteAlumno($alumno)
    {
        $id = $alumno->id;
        $this->load->database();
        $this->db->where('id', $id);
        $this->db->delete('alumnos');

    }

    public function validaAlumno($alumno)
    {
        $this->load->database();
        $id = $alumno->id;
        $query = $this->db->query('SELECT COUNT(id) as no_regs FROM asignaciones where asignaciones.id_alumno = '.$id);
        return $query->result();
    }

}